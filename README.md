# Fruits Detection

Count apples, bananas and oranges using object detection with Python as backend and Reactjs as frontend

## Project structure

    ├── backend 
    │   ├── app.py
    │   ├── model
    │   │   ├── label_map.pbtxt
    │   │   ├── pipeline.config
    │   │   └── saved_model
    │   │       ├── assets
    │   │       ├── label_map.pbtxt
    │   │       ├── saved_model.pb
    │   │       └── variables
    │   │           ├── variables.data-00000-of-00001
    │   │           └── variables.index
    │   └── requirements.txt
    ├── frontend
    │   ├── package.json
    │   ├── public
    │   │   ├── favicon.ico
    │   │   ├── index.html
    │   │   └── manifest.json
    │   └── src
    │       ├── components
    │       │   ├── Predictions
    │       │   │   ├── Prediction
    │       │   │   │   └── Prediction.js
    │       │   │   └── Predictions.js
    │       │   └── Webcam
    │       │       ├── Webcam.css
    │       │       └── Webcam.js
    │       ├── containers
    │       │   └── App.js
    │       ├── index.js
    │       └── index.scss
    └── README.md

## Requirements

    - python=3.7
    - npm
    - webcam
    - web browser

## Installation and initalization

    - Backend Server (Python)
        1. Navigate to backend folder and open terminal
        2. Install required python libraries by executing following command:
            `python -m pip install -r requirements.txt`
        3. Start python server
            `python app.py`

    - Frontend server (ReactJS)
        1. Navigate to frontend folder and open terminal
        2. Install required npm packages by executing following command:
            `npm i`
        3. Start server
            `npm start`

## Validate setup

    - Visit <http://localhost:3000> on web browser and prediciton UI should be visible
    - Visit <http://localhost:5000> on web browser and below message should get displayed:

        *Fruits Detection backend API is running*

## Usage

    - Open any web browser and hit following hyperlink
        <http://localhost:3000>
    - Make sure to provide all the necessary permissions to browser for webcam access
    - Position objects to be detected near webcam such that objects are clearly visible
    - Click on submit when you are positioned correctly
    - Check output predictions on right side in web application

## Approach

    - Model is built on top of Tensorflow 2.0 Object Detection API
    - As the end use case is not known and priorities are not set for any particular metric such as accuracy or latency, below model is selected since it provides a relatively good trade-off between performance and speed.
        **SSD ResNet50 V1 FPN 640x640**
    - Model provided with this appliation is the result of transfer learning with SSD ResNet50 Selected as baseline
    - First iteration of model training with default configurations didn't yield satisfatory results.
    - Annotations are manually validated by labelImg Annotation tool
    - Corrected annotations as there were wrongly labelled, missing annotations
    - Observations on provided dataset:
        - 3 objects to be detected: Apple, Banana, Orange
        - Small sample size
        - Objects are big in size
        - Objects have minute colour shades variation
        - Only few images are real, most of them are graphics/animated
        - Most of the images have high brightness/contrast
    - Given the observations on provided dataset, relevant augmentation techniques have been used in model training to reduce bias and making model more robust in unseen environment
    - Model training was targeted for 10,000 steps but stopped when observed that it is not converging any further.
    - Individual object count is displayed on UI and not predicted bounding boxes as most of the applications of object detection are related to counting of it and it is infeasible to count those based on boxes when multiple objects are present in single instance.
