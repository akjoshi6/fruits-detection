import os
from flask import Flask, jsonify, request
from flask_cors import CORS

from imageio import imread
import numpy as np
import tensorflow as tf
from io import BytesIO

from PIL import Image
import re, time, base64

from random import randint

app = Flask(__name__)
CORS(app)

PATH_TO_MODEL_DIR = os.path.dirname(os.path.realpath(__file__)) + '/model'
PATH_TO_SAVED_MODEL = os.path.join(PATH_TO_MODEL_DIR, "saved_model")

labels= {   
            '1':'apple',
            '2' : 'banana',
            '3': 'orange'
        }
        


def getI420FromBase64(codec):
    """ Convert image from a base64 bytes stream to an image. """
    base64_data = re.sub(b'^data:image/.+;base64,', b'', codec)
    byte_data = base64.b64decode(base64_data)
    image_data = BytesIO(byte_data)
    img = Image.open(image_data)
    return img


@app.route('/')
def homepage():
    return 'Fruits Detection backend API is running'


@app.route('/detection', methods=['POST'])
def detection():
    request.get_data()
    
    # Load in an image to object detect and preprocess it
    img_data = getI420FromBase64(request.data)
    x_input = np.expand_dims(img_data, axis=0) # add an extra dimention.

    tf_results_det = detect_fn(x_input)

    num_detections = int(tf_results_det.pop('num_detections'))
    detections = {key: value[0, :num_detections].numpy()
                   for key, value in tf_results_det.items()}
    detections['num_detections'] = num_detections

    threshold = 0.70

    predict_list=detections["detection_classes"].astype(int).tolist()
    label=[]

    for i in range(num_detections):
        new_item = {}
        if detections["detection_scores"][i] > threshold:
            prediction_label = str(predict_list[i])
            obj_name = labels[prediction_label]

            new_item = {'id': randint(0, 100000),
                        'class': obj_name,
                        'count': 1}

            label.append(new_item)
    print(label)
    print(f"No of predictions:{len(label)}")
    return jsonify(label)


if __name__ == '__main__':

    detect_fn = tf.saved_model.load(PATH_TO_SAVED_MODEL)

    app.run()
