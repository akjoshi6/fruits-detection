import React from 'react';

import Prediction from './Prediction/Prediction';

const Predictions = (props) => {
    return (
        <div>
            <div>
                <table className='bx--data-table-v2'>
                    <thead>
                        <tr>
                            <th>
                                <span className='bx--table-header-label'>
                                    Class
                                </span>
                            </th>
                            <th>
                                <span className='bx--table-header-label'>
                                    Count
                                </span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {props.predictions.map(prediction => {
                            return <Prediction 
                            key={prediction.id}
                            class={prediction.class}
                            count={prediction.count}/>
                        })}
                    </tbody>
                </table>
            </div>
        </div>
        
    )
}

export default Predictions