import React from 'react';

const Prediction = (props) => {
    return (
        <tr onClick={props.click}>
            <td>{props.class}</td>
            <td>{props.count}</td>
        </tr>
    )
}

export default Prediction