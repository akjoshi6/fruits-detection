import React, { Component } from 'react';

import WebcamCapture from '../components/Webcam/Webcam'
import Predictions from '../components/Predictions/Predictions'


class App extends Component {
  state = {
    predictionsArray: []
  }
  
  updatePredictionStateHandler = (dataFromChild) => {
    const totalPredictions = dataFromChild;
    
    let countHolder = {};
    totalPredictions.forEach(function (d) {
      if (countHolder.hasOwnProperty(d.class)) {
        countHolder[d.class] = countHolder[d.class] + d.count
      } else {       
        countHolder[d.class] = d.count
      }
    });

    let finalPredictions = [];
    
    for(let prop in countHolder) {
      finalPredictions.push({class: prop, count: countHolder[prop]});   
    }

    this.setState({predictionsArray: finalPredictions});
  }

  render() {

    return (
        <div className='bx--grid'>
          <div className='bx--row'>
            <div className='bx--col-xs-6'>
              <WebcamCapture 
              callbackFromParent={this.updatePredictionStateHandler}/>
            </div>
            <div className='bx--col-xs-6'>
              <Predictions
                predictions={this.state.predictionsArray}/>
            </div>
          </div>
        </div>

    );
  }
}

export default App;
